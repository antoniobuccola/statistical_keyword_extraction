### Environment Setup

Tested with python 3.9
  - pip install -r requirements.txt
    
    * employed modules 
    
      pandas (1.3.2)
      numpy (1.20.2)
      bs4 (4.9.3)
      lxml (4.6.3)
      nltk (required 3.6.2)
      spacy (required 3.0.6)
      gensim (required 4.0.1)
      python-Levenshtein (0.12.2)
      networkx (2.4)
      scipy (1.6.2)
      scikit-learn (0.24.2)
      matplotlib (3.4.3)
      seaborn (0.11.2)
    
  - python -m spacy download it_core_news_lg  
  
  - export NES_FILE='path/to/CRACK_specific_named_entities.csv'
    NOTE: there is an example of such file in data/

## Usage

#### HTML marker cleaning

python scripts/CRACK_cleaner_HTML_markers.py --raw_dataset path/to/raw_dataset/raw_file.csv --cleaned_dataset: path/to/cleaned/cleaned_file.pkl  

--raw_dataset: input file, default: ./raw_file.csv

--cleaned_dataset: output file, default: ./cleaned_file.pkl

#### Keyword / Keyphrases extraction

python scripts/CRACK_keyword_extractor.py parameters.json --cleaned_dataset path/to/cleaned_dataset/cleaned_file.pkl  --keywords_dataset path/to/keywords_dataset/keywords_file.pkl --category YOUR_CATEGORY --narticles 

parameters.json: file containing the parameters of the algorithm. Example: CRACK_parameters_config.json

--cleaned_dataset: input file, default: ./cleaned_file.pkl

--keywords_dataset: output file, default: ./keywords_file.pkl

--category: one of the 'category_description' values in the cleaned dataset (default: no specific category)

--narticles: number of articles to be analyzes (int, default: all)

NOTE: a keywords_file.csv will also be created to easily eye - check the results as a spreadsheet

### HTML marker cleaning - description

./scripts/CRACK_cleaner_HTML_markers.py

The article contains HTML markers. Before extracting keywords from it, cleaning of HTML markers is mandatory. 
This is performed by parsing the raw article (i.e. the web page of the article) with BeautifulSoup. 
The HTML cleaner procedure is designed for the articles of Blasting News (https://www.blastingnews.com/).
The text fed into the keyword extractor is the body of the article, headlines included.

### Keywords / Keyphrases extraction - description

./scripts/CRACK_keyword_extractor.py

## Parameters

* **max_ngram_size**: maximum size of the extracted ngram, that is a candidate keyword (CK). 
                      Named Entities (NE) are not restricted to this limit.

* **bonus_entities**: applied to score if a CK is a NE

* **num_sentences**: number of sentences to be considered as first

* **bonus_first_sentences**: applied to score if a CK is found in the first sentences. 
                             The score of CKs not in first sentences is multiplied by 2/(bonus_first_sentences + stagger + 1)
                             i.e. CKs not in first sentences are penalized.

* **stagger**: reduce the **bonus_first_sentences** by a factor (1 - stagger * I), where I is the index of the first sentences (I = 0 ... **num_sentences** - 1)

* **num_topics**: number of topics gensim's Latent Dirichlet Allocation (LDA) is allowed fo a given article

* **num_words**: number of words each found topic contains 

* **bonus_gensim**: applied to scoring if a CK contains a word found by LDA

* **summary_sentences**: number of sentences the extractive summary of the article, obtained with PageRank, is composed of

* **bonus_summary**: applied to scoring if a CK is contained in the summary. Partial match is allowed for named entities

* **chars_per_part**: number of characters of a part of the article

* **n_parts**: number of parts of the article considered

* **min_len**: words having characters less than this parameter are ignored

* **word_limit**: first CKs with highest score are given as keyphrases

## Scoring:

* **S**: similarity of CK with the article body

* **freq**: frequency of CK in the article body; frequency of K is updated with the frequency of smaller ones and dividing by the number of them

* **n**: number of words CK is composed of 

Scoring formula in ./scripts/functions.py

