# Log Book of updates for CRACK / TEKE

#### Observations (till last update)

* Stagger increase factor = 10

* Sport-dealing articles (football, tennis...) have a lot of named entities, so give too big a **bonus_entity** may be risky

* Poor similarity among K's of body and titles sometimes occur because the titles are poorly written, that is they do not properly summarize the article, or even because the body lacks of internal coherence... there are many issues about this not related to the algorithm

* 'Oroscopo' (horoscope) appears only one time at the beginning. Unless a very dedicated treatment is given to this word, it is difficult to get

* Some Out Of Vocabulary (OOV) words would be good K's, but they are very hard to get due to the low similarity

  EDIT (07/07/2021): attempt to a min/max normalization if the similarity of a named entity is below than the trimmed mean of the similarities of all found named entities

* 'Una Vita' (Italian title of the Spanish soap opera 'Acacias 38') is almost impossible to catch, since it is short as it is composed by an italian stopword ('Una') and it appears only one or twice throughout the body of the article. The same occurs for 'Il Segreto' ('El Secreto de Puente Viejo').
 
 EDIT (23/06/2021): 'Il Segreto' has been found as a named entities
 
 EDIT (21/06/2021): Testing the possibility to include them by choosing entities starting with an article
 
 EDIT (18/06/2021): 'Vita' and 'Segreto' could still be found, providing a useful "K" (just add an article)
 
## Last Update: 28/09/2021

#### Parameters (LAST EDIT: 14/07/2021)

**max_ngram_size**: 3

**num_sentences**: 5

**bonus_entities**: 3

**bonus_first_sentences**: 5

**num_topics**: 10

**num_words**: 10 

**bonus_gensim**: 2

**min_len**: 2

**summary_sentences**: 3 

**bonus_summary**: 2.5

**word_limit**: 5

**stagger**: 0.1

**chars_per_part**: 400

**n_parts**: 5


## Code Updates:

<h2> 28/09/2021 </h2>

* Only the first found TV program in the article body can be retrieved as a named entities

<h2> 17/09/2021 </h2>

* If any of the words 'oroscopo' or 'previsioni' is found in the body of the article and a month is found too, then a keyword 
  is made up  by the month and one of the two aforementioned words

<h2> 15/09/2021 </h2>

* Small modifications in named entities recognition

* Added 'neanche' in stopwords.

<h2> 14/09/2021 </h2>

* for tv programs, added a list, to be updated as new ones come out, such that, if it is found in the text, 
  it is considered as a named entity and gains a bonus = **bonus_entity** * **bonus_special**

* Similarity among keywords will be now computed after complete cleaning each string resulting from the join and numbers in it

<h2> 10/09/2021 </h2>

* Added replacements: fb --> facebook

* Fixed a bug when min/max normalizing the similarity for named entities

<h2> 09/09/2021 </h2>

* Added determination of the pair of keyword body - title + subtitle havin maximum similarity (keywords_with_best_similarity)

* Added determination of the keyword having the maximum single SEO score (keyword_with_best_SEO)

* Fixed calculation of total SEO score and similarity

<h2> 01/09/2021 </h2>

* Rejection of entities ending with a stopword

* Fixed problem with spaCy and word multiple spacing

* Digits are removed only when calculating similarity among keywords

* Test against body including headlines

<h2> 31/08/2021 </h2>

* 'anticipazioni' is now a special word (high bonus)

* 'enne' is now a stopword (originating from the removal of digits when dealing with people's age, for ex. '18enne')

<h2> 27/08/2021 </h2>

* All digits are removed from text before looking for keywords (degrades the similarity)

* Similarity among keywords from body and title + subtitle is computed with keywords as is (no decapitalization)

<h2> 16/07/2021 </h2>

* Fixed stemmatization check to avoid repetitions in keywords

<h2> 15/07/2021 </h2>

* Added 'utente' ('user'), piattaforma ('platform'), 'funzione' ('function') to stopwords

* Fixed assignment of **bonus_entity**

* Added 'sondaggi' ('surveys') to specials

* Removed from ngrams tokens at the beginning with POS = NUM or CCONJ  and at the end with POS = NUM, CCONJ or VERB

* Removed '(', ')' from punctuation: it helps NER

<h2> 14/07/2021 </h2>

* Modified penalization of non-first sentences: 1/bonus --> 1 - stagger 

* Pruned selection and refined cleaning of named entities

* Pruned condition for final_keyword selection

* Pruned weighting_function

<h2> 13/07/2021 </h2>

* IDF factor is applied to all ngrams

* Added factor IDF-like (Inverse Document Frequency) to frequency of named entities to mitigate rare-occurring named entities 

* Named entities are now allowed to start with an ADV

<h2> 12/07/2021 </h2>

* Fixed bug when calculating frequency using body and headlines

* spaCy == 3.0.6 is required (some issues in NER on the test articles)

<h2> 09/07/2021 </h2>

* Size of the ngram does not take into account articles

* Added 'passeggero' ('passenger'), 'argomento' ('topic'), 'produzione' ('production') to stopwords

* Fixed unigram stemmatization

<h2> 08/07/2021 </h2>

* Included NUM tokens

* Penalization is NOT applied to words not inside the extractive summary

* Rejected named entities having single 'l' or 'd' (sort of "remainder" after the removal of quotes in italian language) or an article in it (i.e. only first token can be an article)

* Added 'agenzia' ('company') to stopwords

* Rejected named entities containing a VERB or an AUX

* Set **min_len** = 2

<h2> 07/07/2021 </h2>

* Penalization is applied to words not in the extractive summary

* Fixed proper assignment of **bonus_summary**

* Fixed bug during stemmatization

* Reset of **bonus_summary** is applied if the expression is not in the summary after selection

* Corrected typo causing the assignment of **bonus_summary** to all words, regardless being truly in the summary or not

<h2> 06/07/2021 </h2>

* Added extractive summarization: if the ngram is in the summary or a named entity partially matches in the summary then multiply the score by a bonus

* Rejection of tokens for ngram extraction with a number of characters less than min_len

* Similarity is computed as the maximum value among these possibilities:
 
  S = similarity(decapitalized ngram, decapitalized cleaned body)
  S = similarity(decapitalized ngram, cleaned body)
  S = similarity(ngram, decapitalized cleaned body)
  S = similarity(ngram, cleaned body)

* Added 'maestro' ('teacher' or 'master' depending on the context) and declinations, 'settimana' ('week'), 'protagonista' ('main character') to stopwords

<h2> 05/07/2021 </h2>

* Unigrams are stemmatized before calculating the frequency (avoids consider singular and plural as different words)

* Normalization of the frequency is not applied for named entities composed only by a unigram

* Rejected ngrams (extracted from text or named entities) sharing more than a fraction of words with already found K's

* Removed the last token of the K's if it is a (derived from a) VERB or a CCONJ

* Partial agreement in headlines is allowed for named entities
 
* Added 'vero' ('true') and declinations, 'fronte' ('front' or 'forehead' depending on the context) to stopwords

<h2> 02/07/2021 </h2>

* Calling pretty_print_keywords will display the score normalized to maximum value and mapped in (0, 100)

* Frequency now keeps into account the headlines (text among 'h2' HTML markers). The weight of this word is determined by the **bonus_headlines** parameter

* Frequency of named entities as potential K's is now computed updating for smaller ngrams and dividing by the sqrt(number) of them

* Modified remove_headlines: the headline is removed from the text if it contains at least two words

* Bug fixing: punctuation before ngram extraction must be included in tokens to construct ngrams. Rejection occurs later

* Added in stopwords: 'davvero' ('really'), 'senz' (contraction for 'senza', that is 'without'), 'giornata' e 'giorno' (synonims, 'day'), 'notizia' ('news')

* Removed 'lavoro' ('job' or 'work', depending on the context) from stopwords

<h2> 01/07/2021 </h2>

* Tokens for ngrm composition are extracted befor calling improved_ngrams_extraction

* Added improved_ngrams_extraction: ngrams can now contain punctuation marks during extraction, which are neglected for the determination of the size. Example N1 = 'Lion, big cat' is a trigram (n=3), as well as N2 = 'Lion likes cats'

 However, if they do have marks, like N1 in the example, they are rejected.

* Stagger is increased by a factor when analysing title & subtitle (not a proper parameter, check the notebook)

* Added bonus_sentence_staggering in functions.py: the **bonus_first_sentences** is reduced according to the index of the first sentence containg the ngram

* Added neighborhood in constant.py. If a smaller ngram is inside any of the (bigger) ones in the neighborhood, it is removed

* Fixed typo: for ngram in first_sentences_cleaned >>>> for cleaned_ngram in first_sentences_cleaned

<h2> 30/06/2021 </h2>

* Updated the stopwords

* Changed the calculation of similarity due to a different treatment of cap-/decap- italized words:
  
  if the candidate expression is a named entities that contains at least one PROPN, compute the similarity between the expression and the cleaned text without de-capitalization
  
  otherwise, de-capitalize both before calculating the similarity

<h2> 29/06/2021 </h2>

* Begun smart replacer to substitute some words with other so that the meaning is kept unchanged and the similarity is better computed. 

  Example: 'televisione' (italian for 'television') for 'tv'/'Tv'

* Found a bug due to spaCy: the similarity de-capitalized expressions may result 0. It does not occur when de-capitalization is not applied

* Added 'el' and 'the' articles in constants.py to keep into account some foreign named entities

* Considered 15 articles from 10 different categories. Two different ways to extract K's from them: **bonus_gensim** = 1 and
**bonus_gensim** > 1. Fit with a gaussian function of the similarity among K's from body and K's from title + subtitle.

* Fixed bug causing some words to disappear if **bonus_gensim** > 1

<h2> 28/06/2021 </h2>

* Rejected named entities having more than 4 tokens, all of them being PROPN

* LDA words are now retrieved once before analyzing the article in search of K's

* Fixed assignment of the **bonus_gensim**

<h2> 25/06/2021 (my B-day!)</h2>

* Introduced **bonus_gensim** to candidate K's

* Added parameters for gensim: **num_topics** and **num_words**

* Added gensim_words to /scripts/functions.py

<h2> 23/06/2021 </h2>

* Fixed updating frequency of named entities: words smaller than **min_len**  did not contibute

* After ranking of candidate K's, removed ngram if it is inside its precedent or consecutive

* Changed the scoring function: **score = S^2 * freq^2 * n * bonus_entities * bonus_first_sentences**

* Fixed selection of named entities starting with a DET

* Removed 'Text inside quotation marks (") are now considered named entities': it can be mismatched with direct speech 

* Fixed bug causing the non assignment of the **bonus_first_sentences** in some cases

* Added penalization for espressions not found in the **num_sentences** sentences

<h2> 22/06/2021 </h2>

* Text inside quotation marks (") are now considered named entities 

* Changed the scoring function: **score = S^3 * f * n^2 * bonus_entities * bonus_first_sentences**

* Selection of the first **word_limit** K's is performed when evaluating the metrics

* Sentence tokenization can be performed using spacy or NLTK

* Search of named entities sentence by sentence, so spaCy does not add spurious words to the entity up during the recognition 

<h2> 21/06/2021 </h2>

* Accepted named entities are the ones starting with a NOUN, PROPN, ADJ, DET

* Entities can now start with an adjective (ADJ) or an article (DET)

<h2> 18/06/2021 </h2>

* Changed the scoring function: **S^2 * sqrt(f) * n * bonus_entities * bonus_first_sentences**

* Fixing bugs due to the new treatment for the frequency

* Frequency update is performed only for named entities

* Removed repetitions among named entities and ngrams

* Selected entities starting with a noun 

* Fixed bug causing the repetitions of entities

<h2> 16/06/2021 </h2>

* Added possibility to generate a dataset of articles balanced w.r.t. the categories

* Frequency of potential K's is now computed updating for smaller ngrams (as before) and dividing by the number of them

* Checked the behaviour of the algorithm on a batch of randomly chosen articles

* Added calculation of the similarity among K's extracted from the body and the ones from the titles

* Size of K's is computed ignoring stopwords in named entities and words shorter than **min_length**

* Frequency is now computed without updating for smaller ngrams

* Added separate function to compute the similarity among the K's from body and from titles

* Trial & error different scoring functions AND "fine - tune" **bonus_entity** and **bonus_start**

* Restored 'MISC' entities: many entities are labelled as such and some of them are truly informative

<h2> 15/06/2021 </h2>

* Rejected entity if it has 'MISC' label (may be everything, thus not so informative)

* Computed the similarity among the K's found in the body and the K's found in title & subtitle

* Rejected ngrams starting with a stopword

* Fixed bug causing named entites to be included in bigger n-grams

* Fixed bug raising empty scoring parameters tuple in output

* Checked the application of bonus_start

* Removed stopwords when considering the size of K

* Fixed the determination of the frequency of the candidate K


<h2> 14/06/2021 </h2>

* Saved scoring parameters in a tuple toghether with K

* Added separate scoring function

* Code organization (notebooks, functions, constants)

* First commit