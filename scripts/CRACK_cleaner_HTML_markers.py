import re
import sys
import os
import argparse
import json

import pandas as pd

from functions import extract_article_parts

parser = argparse.ArgumentParser(description = """CRACK - HTML cleaner script""", epilog = "See you later")

parser.add_argument('--raw_dataset', 
                    type = str, 
                    default = "raw_file.csv",  
                    help = "path/to/raw_dataset/raw_file.csv")

parser.add_argument('--cleaned_dataset',  
                    type = str, 
                    default = "cleaned_file.pkl", 
                    help = "path/to/cleaned_dataset/cleaned_file.pkl")

args = parser.parse_args()

raw_path = args.raw_dataset
try:
    dataset = pd.read_csv(raw_path, index_col = 0)
except Exception as E:
    print(E,'-', f'File {raw_path} not found')
          
dataset['article_contents'] = dataset['news_body'].apply(extract_article_parts)

html_keys = dataset['article_contents'].iloc[0].keys()

for key in html_keys:
    dataset[key] = dataset['article_contents'].apply(lambda d: d[key])

cols_to_keep = ['publish_from', 'category_description', 'channels', 'hits_organic', 'hits_social',
                'news_main_id', 'news_title', 'news_subtitle', 'news_authors_id', 'image_description', 
                'headlines', 'bold_words', 'clean_body', 'whole_text', 'links', 'num_social_links']

cleaned_path = args.cleaned_dataset
dataset[cols_to_keep].to_pickle(cleaned_path)