import warnings
warnings.filterwarnings("ignore")

import argparse
import re
import os
import json

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from constants import *
from functions import * 

parser = argparse.ArgumentParser(description = """CRACK - Keyword extractor""", epilog = "See you later")

parser.add_argument('parameters',
                    type = str, 
                    default = None,
                    help = "file.json containing the parameters to be fed to the algorithm")

parser.add_argument('--cleaned_dataset', 
                    type = str, 
                    default = "cleaned_file.pkl",  
                    help = "path/to/cleaned_dataset/cleaned_file.pkl")

parser.add_argument('--keywords_dataset',  
                    type = str, 
                    default = "keywords_file.pkl", 
                    help = "path/to/keywords_dataset/keywords_file.pkl")

parser.add_argument('--category',
                    type = str, 
                    default = None,
                    help = "One of the 'category_description' values in the cleaned dataset")

parser.add_argument('--narticles',
                    type = int, 
                    default = 0,
                    help = "Number of articles to be analyzes (default: all)")

args = parser.parse_args()

dataset = args.cleaned_dataset
category = args.category
num_of_articles = args.narticles 

output = args.keywords_dataset

ds = pd.read_pickle(dataset)

print('Preparing dataset')

if category is None:
    if num_of_articles > 0:
        df = ds[:num_of_articles]
    else:
        df = ds
else:
    df = ds[(ds['category_description'] == category)][:num_of_articles]

rows = df.shape[0]
if rows > 0:
    print('Total articles:', df.shape[0])
else:
    raise Exception('Dataset must be non empty. Check your inputs')

# getting date and time time information
df['publish_from'] = pd.to_datetime(df['publish_from'])

df['publish_year']    = df['publish_from'].dt.year
df['publish_month']   = df['publish_from'].dt.month
df['publish_day']     = df['publish_from'].dt.day
df['publish_weekday'] = df['publish_from'].dt.day_name()
df['publish_hour']    = df['publish_from'].dt.hour
df['publish_minute']  = df['publish_from'].dt.minute

# parameters for TEKE - Tech Engines Keyword Extractor
with open(args.parameters, mode = 'r') as f:
    parameters = json.load(f)  
    
    max_ngram_size = parameters["max_ngram_size"]
    
    bonus_entities = parameters["bonus_entities"]
    
    num_sentences = parameters["num_sentences"]
    bonus_first_sentences = parameters["bonus_first_sentences"]
    stagger = parameters["stagger"] 
    
    bonus_gensim = parameters["bonus_gensim"]
    num_topics = parameters["num_topics"]
    num_words = parameters["num_words"]
    
    summary_sentences = parameters["summary_sentences"]
    bonus_summary = parameters["bonus_summary"]
    
    min_len = parameters["min_len"]
    
    word_limit = parameters["word_limit"]

assert bonus_entities >= 1, "Bonus of entities must be greater than or equal to 1"
assert num_sentences >= 1, "Number of sentences to be considered first must be greater or equal than 1"
assert bonus_first_sentences >= 1, "Bonus of first sentences must be greater than or equal to 1"
assert bonus_gensim >= 1, "Bonus of words fpund by gensim must be greater than or equal to 1"
assert bonus_summary >= 1, "Bonus for the extractive-text summary must be greater than or equal to 1"
assert stagger < 1, "Stagger must be less than 1"
assert min_len > 1, "Minimum characters composing a word must be 2"
assert word_limit >= 1, "One or more keywords are required"


# text processing for keyword extraction
df['first_n_chars'] = df['whole_text'].apply(lambda t: t[:300])

option = 'nltk'
df['sentences'] = df['whole_text'].apply(lambda text: sentence_tokenization(text, option = option))

# Latent Dirichlet allocation with gensim
df['gensim_words'] = df['whole_text'].apply(lambda text: gensim_words(text, 
                                                                        min_len, 
                                                                        num_topics, 
                                                                        num_words))

df['keywords'] = df.apply(lambda x: teke_sim(text = x['whole_text'], 
                                             sentences = x['sentences'], 
                                             num_sentences = num_sentences,
                                             headlines = x['headlines'],
                                             max_ngram_size = max_ngram_size,
                                             bonus_entities = bonus_entities,
                                             bonus_first_sentences = bonus_first_sentences,
                                             words_from_gensim = x['gensim_words'],
                                             bonus_gensim = bonus_gensim,   
                                             min_len = min_len,
                                             stagger = stagger, 
                                             summary_sentences = summary_sentences, 
                                             bonus_summary = bonus_summary),
                          axis=1)

df['centrality'] = df.apply(lambda x: score_quality(x['news_title'], 
                                                    x['news_subtitle'],                                                                                                             x['first_n_chars'],
                                                    x['keywords'],
                                                    min_len = min_len,
                                                    limit = word_limit), 
                            axis=1)

df['title_and_subtitle'] = df['news_title'] + '. ' + df['news_subtitle']

df['gensim_words_titles'] = df['title_and_subtitle'].apply(lambda text: gensim_words(text, 
                                                                                     min_len, 
                                                                                     num_topics, 
                                                                                     num_words))

df['sentences_titles'] = df['title_and_subtitle'].apply(lambda text: sentence_tokenization(text, option = option))

df['keywords_title_and_subtitle'] = df.apply(lambda x: teke_sim(x['title_and_subtitle'], 
                                                                x['sentences_titles'], 
                                                                num_sentences = num_sentences,
                                                                headlines = x['headlines'],
                                                                max_ngram_size = max_ngram_size,
                                                                bonus_entities = bonus_entities,
                                                                bonus_first_sentences = bonus_first_sentences,
                                                                words_from_gensim = x['gensim_words_titles'],
                                                                bonus_gensim = bonus_gensim,
                                                                min_len = min_len,
                                                                stagger = 10*stagger,
                                                                summary_sentences = summary_sentences, 
                                                                bonus_summary = bonus_summary),
                                             axis=1)


# total hits generated by the article
df['hits_total'] = df['hits_social'] + df['hits_organic']

# switch from month number to proper month
df['publish_month_calendar'] = df['publish_month'].apply(lambda m: nums_to_months[m])

# max SEO score associated to a keyword
df['max_SEO'] = df.apply(lambda r: keyword_with_best_SEO(keywords_body = r['keywords'], 
                                                         min_len = min_len,
                                                         word_limit = word_limit,
                                                         title = r['news_title'], 
                                                         subtitle = r['news_subtitle'], 
                                                         body = r['whole_text'],                                                 
                                                         first_chars = r['first_n_chars'], 
                                                         headlines = r['headlines'], 
                                                         channels = r['channels'], 
                                                         links = r['links'], 
                                                         image_description = r['image_description']),
                         axis = 1)

# calculation of SEO score 
df['SEO_score'] = df.apply(lambda r: SEO_quality(keywords = r['keywords'], 
                                                 min_len = min_len, 
                                                 limit = word_limit, 
                                                 title = r['news_title'], 
                                                 subtitle = r['news_subtitle'], 
                                                 body = r['clean_body'], 
                                                 first_chars = r['first_n_chars'], 
                                                 headlines = r['headlines'], 
                                                 channels = r['channels'], 
                                                 links = r['links'], 
                                                 image_description = r['image_description']),
                           axis = 1)

# max similarity associated to a couple of keyword body - title + subtitle
df['max_simil'] = df.apply(lambda x: keywords_with_best_similarity(x['keywords'],
                                                                   x['keywords_title_and_subtitle'],  
                                                                   limit = word_limit),
                           axis=1)

# calculation of similarity 
df['simil_Ks'] = df.apply(lambda x: similarity_among_Ks(x['keywords'],
                                                        x['keywords_title_and_subtitle'],
                                                        limit = word_limit),
                          axis=1)

# used for faster checks
df['simple_keywords_body'] = df['keywords'].apply(lambda l: [k for k, *s in l[:word_limit]])
df['simple_keywords_title_and_subtitle'] = df['keywords_title_and_subtitle'].apply(lambda l: [k for k, *s in l[:word_limit]]) 

traffic = df.drop(columns = ['publish_from', 'first_n_chars', 'sentences', 'gensim_words', 
                             'gensim_words_titles', 'sentences_titles'])
   
traffic.to_pickle(output)

traffic['similarity'] = traffic['simil_Ks']

cols_fast_check = ['news_main_id', 'news_title', 'news_subtitle', 'whole_text', 
                   'simple_keywords_body' , 'simple_keywords_title_and_subtitle',
                   'SEO_score', 'similarity']

traffic[cols_fast_check].to_csv(output.replace('pkl', 'csv'))

print(traffic.shape)