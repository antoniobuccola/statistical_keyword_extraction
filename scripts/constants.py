import re
import os

has_alpha = re.compile(r'[ldLD]').search

import spacy
import it_core_news_lg
from spacy.matcher import Matcher

import nltk
nltk.download('stopwords', quiet=True)
nltk.download('punkt', quiet=True)

from nltk.corpus import stopwords 
from nltk.stem.snowball import SnowballStemmer

# fraction of words named entities must NOT share one with another to be considered
frac = 0.5

snow = SnowballStemmer(language = 'italian')

# neighborhood: used to search if smaller ngrams are inside nearby bigger ones
near = 2
neighborhood = range(1,near+1)

# special words that may occur only once in specific articles but they have a lot of relevance
specials = {'meteo', 'oroscopo', 'previsioni', 'previsioni zodiacali', 'sondaggi', 'ricetta', 'anticipazioni'}
bonus_special = 300

# list of italian TV programs
try:
    f = open(os.environ['NES_FILE'], mode = 'r', encoding = 'utf-8-sig')
    
except Exception as E:
    print(f'Occurred Exception: {E}. Going to default value')
    f = open('./../../data/CRACK_specific_named_entities.csv', mode = 'r', encoding = 'utf-8-sig')
    
lines = f.readlines()

tv_programs = [re.sub(' +', ' ', re.sub(r'[^\w\s]', ' ', s.strip())).lower() for s in lines]
tv_programs.sort(key = lambda x: len(x.split()), reverse = True)

# punctuation marks
punctuation = "'\'’‘\"“”«»「」:;%\^*~`@#\[\]\{\},.!?-"

nlp = it_core_news_lg.load()
matcher = Matcher(nlp.vocab)

# stopwords for italian languages
stop_words_nltk = set(stopwords.words('italian')) 
stop_words_spacy = nlp.Defaults.stop_words

additional_stops = {'orario', 'bisogno', 'intenzione', 'appena', 'scena', 'ciò','almeno', 'ovvero', 'poiché', 
                    'però', 'opportun', 'tipo', 'parente', 'stare', 'base', 'persona', 'persone',
                    'situazione', 'situazioni', 'retroscena', 'gesto', 'gesti', 'possibile', 'volta', 'volte', 
                    'svolta', 'svolte', 'altro', 'altra', 'altre', 'altri', 'cittadina', 'cittadine',
                    'cittadino', 'cittadini', 'decisione', 'decisioni',  'bensì', 'situazione', 'situazioni', 
                    'chilometro', 'chilometri', 'metro', 'metri', 'base', 'cosiddetto', 'cosiddetti', 
                    'cosiddetta', 'cosiddette', 'davvero', 'senz', 'giornata', 'giorno', 'giorni', 'settimana', 
                    'settimane', 'notizia', 'notizie', 'club', 'azienda', 'funzione', 'funzioni', 'neanche', 
                    'passeggero', 'passeggeri', 'argomento', 'argomenti', 'produzione', 'particolare', 'particolari'
                    'vero', 'veri', 'vera', 'veri', 'fronte', 'maestra', 'maestro','maestri','maestre', 're',
                    'colazione', 'pranzo', 'cena', 'protagonista', 'protagonisti', 'risposta', 'risposte',
                    'personaggio', 'personaggi', 'utente', 'utenti', 'piattaforma', 'piattaforme'} 

# months (italian)

months = ['gennaio', 'febbraio', 'marzo', 'aprile', 'maggio', 'giugno', 
          'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre']

nums = list(range(1,13))

months_to_nums = {num : month for month, num in zip(nums, months)}

nums_to_months = {num : month for month, num in zip(months, nums)}

# days (italian)
days = ['lunedì', 'martedì', 'mercoledì', 'giovedì', 'venerdì', 'sabato', 'domenica']

# word-to-number converter (italian)
wtn_conv = {'uno' : 1, 'due' : 2, 'tre' : 3, 'quattro' : 4, 'cinque' : 5, 
            'sei' : 6, 'sette' : 7, 'otto' : 8, 'nove' : 9, 'dieci' : 10,
            'undici' : 11, 'dodici' : 12, 'tredici' : 13, 'quattordici' : 14, 'quindici' : 15, 
            'sedici' : 16, 'diciassette' : 17, 'diciotto' : 18, 'diciannove' : 19, 'venti' : 20,
            'ventuno' : 21, 'ventidue' : 22, 'ventitre' : 23, 'ventiré': 23, 'ventiquattro' : 24, 
            'venticinque' : 25, 'ventisei' : 26, 'ventisette' : 27, 'ventotto' : 28, 'ventinove' : 29, 
            'trenta' : 30, 'trentuno' : 31}

# matcher for dates
# DD + MM (+ YY, optional)
dates = [{"POS": "NUM"}, {"LOWER": {"IN": months}}, {"POS": "NUM", "OP": "*"}]
matcher.add("DATE", [dates])

# articles encountered so far
articles = {'il', 'la', 'lo', 'i', 'gli', 'le', 'un', 'una', 'uno', 'el', 'the'}

stop_words_all = additional_stops
stop_words_all |= stop_words_nltk
stop_words_all |= stop_words_spacy

# put here the stopwords are considered as such from NLTK/spaCy (with the exception of 'e'), yet you think they are not
not_stop_words = set(['ministro','vita', 'grande', 'e', 'torino', 'anticipo'])
stop_words = set(stop_words_all) - not_stop_words
stop_words = stop_words - articles

# smart replacer
replacer = {'Tv'   : 'televisione',
            'tv'   : 'televisione',
            'anch' : 'anche',
            'fb'   : 'facebook',
            ' ig ' : ' instagram '}