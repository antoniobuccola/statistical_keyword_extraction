from math import isclose, log, sqrt, exp

import re
import collections

from itertools import accumulate, combinations, product

import pandas as pd
import numpy as np
from scipy.stats import trim_mean

from tabulate import tabulate

from nltk.util import ngrams
from nltk import sent_tokenize
from nltk import word_tokenize

import gensim
from gensim.corpora import Dictionary
from gensim.models import LdaModel

from constants import *

from datetime import date

from bs4 import BeautifulSoup

##############################################################################################################
##############################################################################################################

def extract_article_parts(html_text):
    
    soup = BeautifulSoup(html_text, 'lxml')
    
    text_dict = {}
            
    text_dict['headlines'] =  [t.text.replace('\n', ' ').strip() for t in soup.find_all('h2')]
    text_dict['bold_words'] = [bf.text.replace('\n', ' ').strip() for bf in soup.find_all('strong')]
    
    paragraphs = [par for par in [par.get_text() for par in soup.find_all('p')] if 'NOTA DI CORREZIONE' not in par]
        
    try:
        text_dict['clean_body'] = list(accumulate([par + ' ' for par in paragraphs if len(par)>0]))[-1][:-1]
        text_dict['whole_text'] = soup.get_text().replace('\\\n', ' ')
        
        if len(text_dict['headlines']) != 0:
            
            for h in text_dict['headlines']:
                if len(h) > 0:
                    text_dict['whole_text'] = text_dict['whole_text'].replace(h, h + '.')
        
        text_dict['whole_text'] = text_dict['whole_text'].replace('..', '.')
        
    except IndexError:
        print(paragraphs)
        print('HTML text:', html_text)
        print('ID:', ID)
        return {}
        
    text_dict['links'] = [link for link in [link.text.replace('\n', ' ').strip() 
                                            for link in soup.find_all('a')]
                          if link in text_dict['clean_body']] 
    
    text_dict['num_social_links'] = len([x for x in soup.find_all('iframe', {'class' : 'iframe-social'})])
    
    return text_dict
    
##############################################################################################################
##############################################################################################################

def day_differences(publish_year, publish_month, publish_day, dates):
    
    """Calculate the difference among the publish date and the date(s) found inside the body"""
    
    if len(dates) == 0:
        return np.nan
    
    publish_date = date(publish_year, publish_month, publish_day)
    
    numeric_month_day = []
    for date_ in dates:
        
        info = date_.split()
        
        _day   = info[0] 
        _month = info[1].lower()
        
        try:
            year = info[2]
        except IndexError:
            year = publish_year
        
        try:
            day = wtn_conv[_day] 
        except KeyError:
            day = _day
                    
        month = months_to_num[_month]
         
        numeric_month_day.append((int(year), int(month), int(day)))
    
    difference = max([(publish_date - date(year, month, day)).days for (year, month, day) in numeric_month_day])
    return difference

##############################################################################################################
##############################################################################################################
##############################################################################################################

def cleaner(text, min_len, rm_stopwords = True, rm_punctuation = True, rm_singlechars = True, rm_whitespaces = True):
    
    """All-in-one function to remove (rm) parts of the text"""
    
    cleaned_text = text
    
    if rm_punctuation:
        cleaned_text = re.sub(r'[^\w\s]',' ', cleaned_text)
        
    if rm_stopwords:
        cleaned_text = ' '.join([word for word in cleaned_text.split() 
                                 if word.lower() not in stop_words and word.lower() not in articles])
       
    if rm_singlechars:
        cleaned_text = ' '.join([w for w in cleaned_text.split() 
                                 if len(w) >= min_len or 
                                 (len(w) < min_len and ((w in ['e', '&']) or 
                                                        (w.lower() in articles) or 
                                                        (w.lower() == 'tv') or
                                                        (w.isnumeric())))])     
    if rm_whitespaces:
        cleaned_text = re.sub(' +', ' ', cleaned_text)
        
    return cleaned_text

def complete_clean(text, min_len):
    
    """Clean the text from everything not needed"""
    
    return cleaner(text, min_len, rm_stopwords = True, rm_punctuation = True, rm_singlechars = True, rm_whitespaces = True)

##############################################################################################################
##############################################################################################################
##############################################################################################################

def ngrams_extraction(text, n, min_len):
    
    """Extract the ngrams from a text.
    Words kept: common and proper nouns, verbs, conjuntions, adjectives"""
    
    _text = complete_clean(text, min_len)

    N_grams = ngrams([token.text for token in nlp(_text) 
                      if token.text.lower() not in stop_words 
                      and token.pos_ in ['NOUN', 'PROPN', 'VERB', 'CCONJ', 'ADJ']], n) 
    
    return [ ' '.join(_gram) for _gram in N_grams]


def improved_ngram_extraction(tokens, n, min_len):
        
    ngrams = []
    
    for i in range(len(tokens)):
        
        len_ = 0
        ngram = ''        
        
        try:
            while len_ < n:           
                if tokens[i] in ngram:
                    i += 1                               
                if tokens[i] in punctuation:
                    ngram += tokens[i]
                    i += 1          
                else:
                    ngram += tokens[i] + ' '
                    i += 1
                    len_ += 1
                    
            ngrams.append(ngram)
                
        except IndexError:
            pass
    
    return [ngram[:-1] for ngram in ngrams 
            if all(i not in punctuation or i == '-' for i in ngram) and ngram.split()[0] != 'e' and ngram.split()[-1] != 'e']

##############################################################################################################
##############################################################################################################
##############################################################################################################

def sentence_tokenization(text, option):
    
    option = option.lower()
    
    if option == 'nltk' or option == 'spacy':
        
        if option == 'nltk':
            sentences = sent_tokenize(text)   
        else:
            sentences = [s.text for s in nlp(text).sents]
            
        return [s for s in sentences if len(s)>=3]
    
    else:
        raise NotImplementedError('Available options: NLTK, spaCy')
        
    

def remove_headlines(body, headlines):
    
    """Remove the headlines from the body of the article"""
            
    for headline in headlines:
        if len(headline.split()) > 2:
            body = body.replace(headline, '')
        
    return body
    
def get_article_from_start_to_end_char(text, start, end):
    
    """Select a part of the text from characters with index start (int) to characters with index end"""
    
    if start<0:
        start = 0
    
    return text[start:end]

def quality_body_parts(text, nchars, max_num_of_parts, weighted_keywords, min_len, limit, append_last_part = False):
    
    """divide the article into n parts, the i-th part consisting in the characters from  i*nchars to (i+1)*nchars
    the n+1 part being from n*chars to end of article, then the centrality of the keywords is evaluated"""
    
    # get the keywords from the list with tuples (keywors, score)
    keywords = [kw for kw, *stuff in weighted_keywords][:limit]
    
    # get the parts of the article
    parts = [get_article_from_start_to_end_char(text, n*nchars, (n+1)*nchars)
             for n in range(0, max_num_of_parts)]
    
    # remove possible empty part
    parts = [part for part in parts if len(part)>0]
    
    # append remaining characters if selected parts do not exhaust all the characters of the article
    if append_last_part:
        if sum([len(part) for part in parts]) < len(text):       
            last_part = text
            for part in parts:
                last_part = last_part.replace(part, '')           
            
            parts.append(last_part)
        
    # conversion to spaCy objects        
    sp_parts = {part: nlp(complete_clean(part, min_len).lower()) for part in parts}
    
    # calculate the centrality for each part
    raw_centralities = {index: sum([nlp(keyword.lower()).similarity(sp_part) for keyword in keywords])
                        for index, (part, sp_part) in enumerate(sp_parts.items())}
     
    centralities = {}
    
    # making a fancy looking dictionary
    
    for index, centrality in raw_centralities.items():
        
        if index == max_num_of_parts:
            
            # define a easy readable key
            key = str(nchars*(max_num_of_parts)+1)+'-'+str(len(text))            
            centralities[key] = (centrality, centrality/len(keywords))
            
        else:
            key = str(nchars*(index)+1)+'-'+str(nchars*(index+1))
            centralities[key] = (centrality, centrality/len(keywords))
        
    return centralities
    
def score_quality(title, subtitle, first_chars, keywords, min_len, limit):
    
    """Compute keywords centrality: sum of the similarity on the found keywords"""
    
    score = 0
    
    num_keywords = len(keywords)
    
    if num_keywords == 0:
        return score 
         
    title_cleaned = complete_clean(title, min_len).lower()    
    subtitle_cleaned = complete_clean(subtitle, min_len).lower()
    
    sp_title_subtitle = nlp(subtitle_cleaned + ' ' + title_cleaned)
                
    low_keyword = [kw.lower() for kw, *stuff in keywords[:limit]]
    sp_keywords = {kw.lower(): nlp(kw) for kw in low_keyword}

    score = sum([sp_kw.similarity(sp_title_subtitle) for kw, sp_kw in sp_keywords.items()])
            
    return score

##############################################################################################################
##############################################################################################################
##############################################################################################################

def select_entities(sentences_no_marks, min_len):
    
    """Named Entity Recognition sentence by sentence"""
    
    sp_sentences = [nlp(_s) for _s in sentences_no_marks]
    
    # get all named entities starting with a noun
    # convert them fom spacy.tokens.span.Span to Python string
    # sort named entities by decreasing number of words              
    
    nes = [ne.text 
           for sentence in sp_sentences
           for ne in sentence.ents 
           if (ne[0].pos_ in ['NOUN', 'PROPN', 'ADJ', 'ADV', 'DET']) and  
              ne[-1].text.lower() not in stop_words and
              len([w.start() for w in re.finditer('\S+', ne.text) 
                   if len(w.group()) == 1 and has_alpha(w.group())]) == 0 and
              all(t.pos_ != 'DET' for t in ne[1:]) and
              not(len(ne) >= 4 and all(t.pos_ == 'PROPN' for t in ne)) and
              len(ne.text) >= min_len] 
    
    text_no_marks = list(accumulate([sent + ' ' for sent in sentences_no_marks]))[-1][:-1] 
    text_no_marks = re.sub(' +', ' ', text_no_marks)
    
    nes.sort(key = lambda x: len(x.split()), reverse = True)
    
    for tv_program in tv_programs:
        results = re.search(tv_program, text_no_marks, re.IGNORECASE)
        if results:
            nes.insert(0, results.group(0))
            break
               
    if len(nes) == 0:
        return nes
    
    named_entities = [nes[0]]
    
    for ne in nes[1:]:
        if all(w not in n for n in named_entities for w in ne.split()) and not ne.isspace():
            ne = re.sub(' +', ' ', ne)
            
            if ne[-1] == ' ':
                ne = ne[-1]
                
            named_entities.append(ne)

    return named_entities

##############################################################################################################
##############################################################################################################
##############################################################################################################

def SEO_quality(keywords, min_len, limit, 
                title, subtitle, body, first_chars, headlines, channels, links, image_description):
    """SEO score calculation"""
    
    seo_score = 0
    
    if len(keywords) == 0:
        return seo_score
    
    title_cleaned = complete_clean(title, min_len).lower()
    first_words_title = [word for word in title_cleaned.split()][:3]
    
    subtitle_cleaned = complete_clean(subtitle, min_len).lower()
    
    low_channels = list(map(lambda s: s.lower(), channels))
    
    low_first_chars = first_chars.lower()
    
    body_cleaned = complete_clean(body, min_len).lower()
    
    if image_description is not None:
        image_description_cleaned = complete_clean(image_description, min_len)
    
    # points assigned due to length of the article
    if len(body) >= 3000:
        seo_score += 15
        
    for kw, *stuff in keywords[:limit]:
        
        kw = kw.lower()
        
        # points assigned if keyword is in title
        if kw in title_cleaned or kw in title.lower(): 
            seo_score += 20
    
        # points assigned if keyword is in the first words of title
        if all(w == first_words_title[i] for i, w in enumerate(kw.split()) if i < len(first_words_title)):            
            seo_score += 7.5 
            
        # points assigned if keyword is in sub title
        if kw in subtitle_cleaned or kw in subtitle.lower():
            seo_score += 2.5
            
        # points assigned if keyword is in first chars of the body of the article
        if kw in low_first_chars or kw in first_chars:
            seo_score += 5
            
        # points assigned if keyword is in at least three headlines 
        n = sum([1 for hl in headlines if kw in hl.lower() or kw in complete_clean(hl, min_len).lower()])
        if n>=3:
            seo_score += 20
            
        try:
            # points assigned if keyword is in at least one of the links
            # and there are at least two links made by at least 4 words        
            if sum([len(re.sub(r'[^\w\s]', '', l).split()) >= 4 for l in links]) >= 2 and \
               sum([1 for l in links if kw in l.lower() or kw in complete_clean(l, min_len).lower()]) >= 1:
                seo_score += 15
                
        except Exception as E:
            pass
        
        # points assigned if keyword frequency is high
        frequency = 100*body_cleaned.split().count(kw)/len(body_cleaned.split())
        if frequency >= 1 and frequency <= 4:
            seo_score += 2.5
        
        try:
            # points assigned if keyword is in the image description
            if kw in image_description_cleaned or kw in image_description.lower():
                seo_score += 2.5
                
        except Exception as E:
            pass 
     
        # points assigned if keyword is in channels
        if kw in low_channels:
            seo_score += 10
        
    return seo_score

##############################################################################################################

def keyword_with_best_SEO(keywords_body, min_len, word_limit, 
                          title, subtitle, body, first_chars, headlines, channels, links, image_description):
    
    """Get the maximum SEO associated to a single keyword/keyphrase"""
    
    seo_scores = {}
    
    title_cleaned = complete_clean(title, min_len).lower()
    first_words_title = [word for word in title_cleaned.split()][:3]  
    
    subtitle_cleaned = complete_clean(subtitle, min_len).lower()
       
    body_cleaned = complete_clean(body, min_len).lower()
    
    low_channels = list(map(lambda s: s.lower(), channels))
    
    low_first_chars = first_chars.lower()
    
    image_description_cleaned = complete_clean(image_description, min_len)
    
    for keyword, *s in keywords_body[:word_limit]:
        
        seo_score = 0
        
        kw = keyword.lower()
       
        # points assigned if keyword is in title
        if kw in title_cleaned or kw in title.lower(): 
            seo_score += 20
    
        # points assigned if keyword is in the first words of title
        if all(w == first_words_title[i] for i, w in enumerate(kw.split()) if i < len(first_words_title)):            
            seo_score += 7.5 
            
        # points assigned if keyword is in sub title
        if kw in subtitle_cleaned or kw in subtitle.lower():
            seo_score += 2.5
            
        # points assigned if keyword is in first chars of the body of the article
        if kw in low_first_chars or kw in first_chars:
            seo_score += 5
            
        # points assigned if keyword is in at least three headlines 
        n = sum([1 for hl in headlines if kw in hl.lower() or kw in complete_clean(hl, min_len).lower()])
        if n>=3:
            seo_score += 20
            
        # points assigned if keyword is in at least one of the links
        # and there are at least two links made by at least 4 words        
        if sum([len(re.sub(r'[^\w\s]', '', l).split()) >= 4 for l in links]) >= 2 and \
           sum([1 for l in links if kw in l.lower() or kw in complete_clean(l, min_len).lower()]) >= 1:
            seo_score += 15
        
        # points assigned if keyword frequency is high
        frequency = 100*body_cleaned.split().count(kw)/len(body_cleaned.split())
        if frequency >= 1 and frequency <= 4:
            seo_score += 2.5
            
        # points assigned if keyword is in the image description
        if kw in image_description_cleaned or kw in image_description.lower():
            seo_score += 2.5
     
        # points assigned if keyword is in channels
        if kw in low_channels:
            seo_score += 10
        
        seo_scores[keyword] = seo_score
        
    seo_scores = sorted(seo_scores.items(), key = lambda item: item[1], reverse = True)
    
    return seo_scores[0][1]

##################################################################################################################

def similarity_among_Ks(Ks_from_body, Ks_from_titles, limit):
    
    """Compute the similarity among two list of keywords"""
    
    kws_body = list(accumulate([word + ' ' for word, *stuff in Ks_from_body[:limit]]))[-1][:-1]  
    kws_body = re.sub(r'\d+', '', complete_clean(kws_body, min_len = 2))
    
    kws_titles = list(accumulate([word + ' ' for word, *stuff in Ks_from_titles[:limit]]))[-1][:-1]
    kws_titles = re.sub(r'\d+', '', complete_clean(kws_titles, min_len = 2))
    
    return nlp(kws_body).similarity(nlp(kws_titles))

##################################################################################################################

def keywords_with_best_similarity(keywords_body, keywords_titles, limit):
    
    """Get the maximum similarity associated to a couple keyword/keyphrase body - title + subtitle"""
    
    kws_body   = [nlp(word) for word, *stuff in keywords_body[:limit]]
    kws_titles = [nlp(word) for word, *stuff in keywords_titles[:limit]]
    
    best_simil = max([kw_body.similarity(kw_titles) for kw_body, kw_titles in product(kws_body, kws_titles)])
    
    return best_simil
    
##################################################################################################################
################################################################################################################## 

from scipy.stats import skew, kurtosis

def stats(col):
    
    v = col.values
    
    print(f"min / Max: {round(v.min(), 3)} / {round(v.max(), 3)}   Mean {round(np.mean(v), 3)}   Std. Dev. {round(np.std(v), 3)}   Median {round(np.median(v), 3)}")
    
################################################################################################################## 

from scipy.stats import spearmanr, pearsonr

def correlation_metrics(x, y):
    pears_coeff, pears_pvalue = pearsonr(x, y)
    spear_coeff, spear_pvalue = spearmanr(x, y)
    
    print(f"Pearson  corr. coeff. {round(pears_coeff, 3)} p-value {round(pears_pvalue, 3)}")
    print(f"Spearman corr. coeff. {round(spear_coeff, 3)} p-value {round(spear_pvalue, 3)}")
    
##################################################################################################################  
##################################################################################################################
##################################################################################################################   

def pretty_print_keywords(weighted_keyphrases, full = True, show_index = True):
    
    """Print a list of tuples in a pretty way"""
    
    data = [(K, score, round(sim, 3), round(freq, 2), size, bonus_entity, bonus_first_sent, bonus_sum, bonus_gensim)
            for (K, score, (sim, freq, size, bonus_entity, bonus_first_sent, bonus_sum, bonus_gensim)) in weighted_keyphrases]
    
    df = pd.DataFrame.from_records(data, 
                                   columns = ['keyphrase', 'score', 'S', 'f', 'size', 'ent', 'sent', 'sum', 'gen'])
        
    if full:
        print(tabulate(df, headers = 'keys', showindex = show_index, tablefmt = "fancy_grid", numalign = "center"))
    else:
        print(tabulate(df[['keyphrase', 'score']], headers = 'keys', showindex = show_index, tablefmt = "fancy_grid", numalign = "center"))
    
def results_from_article(df, num_sentences, word_limit, min_len, index, full = True, show_index = True, other_sents = False):
    
    row = df.iloc[index]
    
    print(f"NEWS ID: {row['news_main_id']}")
    
    print()
    print('Title:', row['news_title'], '\n')
    print('Subtitle:', row['news_subtitle'], '\n')
    
    print('First Sentences:')
    for i, sent in enumerate(row['sentences'][:num_sentences]):
        print(i, '-', sent)
    
    if other_sents:
        print()
        print('Other Sentences:')
        for i, sent in enumerate(row['sentences'][num_sentences:]):
            print(i + num_sentences, '-', sent)
    
    print()
    print('---- Headlines ----')
    
    for h in row['headlines']:
        if h != '':
            print(h)  
                       
    algo = 'TEKE'    
    
    name_kws = f'keywords_{algo}'
    name_seo = f'SEO_{algo}'
    name_cen = f'centrality_{algo}_title_subtitle'
    name_title_sub = f'keywords_{algo}_title_and_subtitle'
    
    weighted_keyphrases_from_body = row[name_kws][:word_limit]
    weighted_keyphrases_from_titles = row[name_title_sub][:word_limit]
    
    Ks_similarity = similarity_among_Ks(weighted_keyphrases_from_body, weighted_keyphrases_from_titles, word_limit)
    
    print()
    print('Similarity kws(body) - kws(titles):', "{:.1f}%".format(100*Ks_similarity))

    print()
    print('++++++++++ Keyphrases from body ++++++++++')
    print()
    pretty_print_keywords(weighted_keyphrases_from_body, full, show_index)
    
    try:
        print()
        print()
        print('**** Words from gensim ****')
        print()
        for k in row['gensim_words_body']:
            print(k, end = ', ')
    except KeyError:
        pass
        
    
    print()
    print()
    print('---- Named Entities ----')
    print()
    for e in row['entities']:
        print(e, end = ', ')
    
    print()
    print()
    print('++++++++++ Keyphrases from title & subtitle ++++++++++')
    print()
    pretty_print_keywords(weighted_keyphrases_from_titles, full, show_index)
    
    """
    print()
    title_subtitle = row[name_cen]
    print('Centrality with title & subtitle:', "{:.2f}".format(title_subtitle), 
      '(Average: {:.3f})'.format(title_subtitle/word_limit))
    
    
    print()
    print('Centrality with parts of article body (0: total, 1: average on number of keyphrases)\n')
    body_parts_centrality = row['centrality_parts']
    
    ctr = pd.DataFrame.from_dict(body_parts_centrality)
    ctr['mean'] = ctr.mean(axis=1)
    ctr['std_dev'] = ctr.std(axis=1)
    print(ctr)

    SEO_score = row[name_seo]
    print()
    print('SEO score:', SEO_score)
    print('Average SEO score:', "{:.2f}".format(SEO_score/word_limit))
    print('SEO scored points / Maximum score (20 x number of expressions): ', 
      "{:.5}".format(row[name_seo]/(20*word_limit)))
    """
    
##################################################################################################################
##################################################################################################################
################################################################################################################## 
       
def scoring_function(parameters):
    
    """Use the scoring parameters to compute the score used for ranking the K's """
    
    (S, freq, n, bonus_entity, bonus_start, bonus_summary, bonus_gensim) = parameters    
    score = S**2 * freq**2 * n * bonus_entity * bonus_start * bonus_gensim * bonus_summary
    
    return score

def tf_idf(ngram, sentences_no_marks):
    
    """Calculate a TF-IDF-term using the sentences of the article"""
    
    num_of_sentences = len(sentences_no_marks)   
    num_of_occurrences = sum([int(any(n == s 
                                      for s in sentence.lower().split()
                                      for n in ngram.lower().split()))
                              for sentence in sentences_no_marks])
    
    #print(ngram, num_of_occurrences)
    
    return log((num_of_sentences + 1)/(num_of_occurrences + 1) + 1)
    

################################################################################################################## 

def sort_dict_by_value(d):  
    
    """Sort a dictionary by its values"""
    
    temp_d = {expression : (scoring_function(parameters), parameters)
              for expression, parameters in d.items()}
    
    sorted_d = dict(sorted(temp_d.items(), key=lambda item: item[1][0], reverse=True))
    
    return sorted_d

##################################################################################################################

def gensim_words(text, min_len, n_topics, n_words):
    
    body = [word for word in cleaner(text, min_len, 
                                     rm_stopwords = True, 
                                     rm_punctuation = True, 
                                     rm_singlechars = False, 
                                     rm_whitespaces = True).split() 
            if nlp(word)[0].pos_ in ['NOUN', 'PROPN', 'VERB']]
    
    dictionary = Dictionary([body])
    bow_corpus = [dictionary.doc2bow(doc) for doc in [body]]
    
    model = LdaModel(corpus = bow_corpus, num_topics = n_topics, id2word = dictionary)
    topics = model.show_topics(num_topics = n_topics, num_words = n_words, formatted = False)

    topics_words = [tp[1] for tp in topics] # list of lists of weighted tuples
    
    d_gensim = {}
    for list_ in topics_words:
        
        for word, weight in list_: 
            
            if body.count(snow.stem(word)) == 1:
                continue
            
            if any(snow.stem(word).lower() in key.lower() for key in d_gensim.keys()):
                
                for key in d_gensim.keys():
                    if snow.stem(word).lower() in key.lower() or word in key:
                        d_gensim[key] += weight
                
            else:
                d_gensim[word] = weight
        
    return list(d_gensim.keys())

##################################################################################################################
##################################################################################################################
##################################################################################################################

def bonus_sentence_staggering(ngram, cleaned_ngram, first_sentences, first_sentences_cleaned, bonus_first_sentences, stagger):
    
    """Reduce the bonus_first_sentences according to the index of the sentence"""
    
    indices_standard = [idx for idx, s in enumerate(first_sentences) 
                        if snow.stem(ngram).lower() in s.lower() or ngram in s]
    
    indices_cleaned  = [idx for idx, s in enumerate(first_sentences_cleaned) 
                        if snow.stem(cleaned_ngram).lower() in s.lower() or cleaned_ngram in s]
    
    len_std = len(indices_standard)
    len_cln = len(indices_cleaned)
    
    if len_std == 0 and len_cln == 0:
        return 2/(bonus_first_sentences + stagger + 1)
    
    if len_std > 0 and len_cln == 0:
        index = indices_standard[0]
        
    elif len_std == 0 and len_cln > 0:
        index = indices_cleaned[0]
    
    else:
        index = min(indices_standard[0], indices_cleaned[0])
    
    B = bonus_first_sentences*(1-stagger*index)
    
    if B < 1:
        return 1
    else:
        return B
    
    
############################################################################################################
############################################################################################################
############################################################################################################

import networkx as nx
import gc

def build_similarity_matrix(sentences, min_len):
    
    d_sents = {i : nlp(complete_clean(s, min_len)) for i, s in enumerate(sentences)}
    
    similarity_matrix = np.zeros((len(sentences),len(sentences)))
    
    for idx1 in range(len(sentences)):
        for idx2 in range(len(sentences)):     
            if idx1 > idx2:
                similarity_matrix[idx1][idx2] = d_sents[idx1].similarity(d_sents[idx2])
                
    return similarity_matrix

def generate_summary(sentences, min_len, top_n):
    
    similarity_matrix = build_similarity_matrix(sentences, min_len)      
    
    sentence_similarity_graph = nx.from_numpy_array(similarity_matrix)
    
    try:
        scores = nx.pagerank(sentence_similarity_graph, max_iter = 1000, tol = 1e-3, alpha = 0.25)
    except Exception as e:
        print('****** ', e)
        print()
        print(sentences[0])
        return " ".join(sentences[:top_n])      
    
    ranked_sentences = sorted(((scores[i],s) for i,s in enumerate(sentences)),reverse=True)
   
    summarize_text = []
    for i in range(min(top_n, len(sentences))):
        summarize_text.append(ranked_sentences[i][1])
        
    del similarity_matrix
    del sentence_similarity_graph
    del ranked_sentences
    
    gc.collect()
    
    return " ".join(summarize_text)

##################################################################################################################
##################################################################################################################

def stem_or_not(w):
    
    """Stem only words not in specials"""
    
    if w in specials:
        return w
    
    else:
        return snow.stem(w)

##################################################################################################################
##################################################################################################################
##################################################################################################################
##################################################################################################################
##################################################################################################################
##################################################################################################################
##################################################################################################################
##################################################################################################################
   

def teke_sim(text, sentences, num_sentences, headlines, max_ngram_size, bonus_entities, bonus_first_sentences, words_from_gensim, bonus_gensim, min_len, stagger, summary_sentences, bonus_summary, printer = False):
    
    """The real keyword extractor"""
    
    # check if a month AND a horoscope related word are present in the text
    
    found_month = False
    the_month = ''
    
    if any(month in text.lower() for month in months) and any(w in text.lower() for w in ['oroscopo', 'previsioni']):
        found_month = True
        the_month = [month for month in months if month in text.lower()][0]        
    
    # remove punctuation from sentences 
    # used for named entity recognition and related processing
    sentences_no_marks = [cleaner(sentence, 
                                  min_len,
                                  rm_stopwords = False, 
                                  rm_punctuation = True,
                                  rm_singlechars = False, 
                                  rm_whitespaces = False)
                          for sentence in sentences if len(sentence) >= min_len]
       
    # get the first sentences    
    first_sentences = sentences[:num_sentences] 
    first_sentences_cleaned = [complete_clean(sentence, min_len) for sentence in first_sentences]
    
    # get the named entities and sort them by number of words
    
    if printer:
        print('Sentence:', sentences_no_marks)
        
    entities = select_entities(sentences_no_marks, min_len)    
    
    # clean the text and create a spaCy objects
    text_cleaned = complete_clean(text, min_len)
    
    sp_text_cleaned_standard = nlp(text_cleaned)
    
    similarities = np.array([nlp(e).similarity(sp_text_cleaned_standard) for e in entities])
    trim_sim = trim_mean(similarities, 0.1)
    
    try:
        _min = min(similarities)
        _max = max(similarities)
    except Exception as E:
        _min = 0
        _max = 0
        
    if printer:
        print('Entities:', dict(zip(entities, similarities)))
        print('Trimmed similarity entites:', trim_sim)
        print(f'min / Max: {_min} / {_max}')
    
    sp_text_cleaned_lowered  = nlp(text_cleaned.lower())
    
    # get the extractive summary
    summary = generate_summary(sentences, min_len, summary_sentences)
    summary_cleaned = complete_clean(summary, min_len)
        
    # extract all the ngrams keeping into account
    # punctuation and not distinguishing among singular/plural unigrams
    # unigrams inside named entities are not changed
    
    extraction_text = cleaner(text.replace("\'", " "), min_len,
                              rm_stopwords=True, rm_punctuation=False, rm_singlechars=True, rm_whitespaces=True)
        
    tokens = [token for token in word_tokenize(extraction_text) 
              if token.lower() not in stop_words and 
              nlp(token)[0].pos_ in ['NOUN', 'PROPN', 'VERB', 'CCONJ', 'ADJ', 'PUNCT', 'NUM']]
    
    if printer:
        print()
        print('******* Before:', tokens)        
                                                     
    unigrams = improved_ngram_extraction(tokens, 1, min_len)
    d_stem = {stem_or_not(t) : [] for t in unigrams if t not in punctuation and all(t not in e for e in entities)}
    
    if printer:
        print()
        print('******* in-between (1):', d_stem.keys())        
    
    for t in tokens:
        if any(t in e for e in entities):
            continue
        try:
            d_stem[stem_or_not(t)].append(t.lower())
        except KeyError:
            continue
    
    if printer:
        print()
        print('******* in-between (2):', d_stem) 
    
    
    d_unigrams = {}
    
    for stem, l in d_stem.items():
        try:
            d_unigrams[stem] = max(l, key = l.count)
        except ValueError:
            continue
            
    if printer:
        print()
        print('******* Middle:', d_unigrams)  
    
    new_tokens = []
    for token in tokens:
        
        if any(token in e for e in entities) or token in punctuation:
            new_tokens.append(token)
            continue
            
        try:
            new_tokens.append(d_unigrams[stem_or_not(token)])
        except KeyError:
            new_tokens.append(token)
            continue
     
    if printer:
        print()
        print('****** After:', new_tokens)
        print()
    
    # extract all the ngrams using the new tokens
    all_ngrams = [ngram for n in range(max_ngram_size, 0, -1) for ngram in improved_ngram_extraction(new_tokens, n, min_len)]

    # get the frequency of each ngram
    counts = collections.Counter(all_ngrams)
    
    # get unique ngrams and sort them by number of words
    considered_ngrams = list(set(all_ngrams))
    considered_ngrams.sort(key = lambda x: len(x.split()), reverse = True)
    
    # dictionary ngram - frequency 
    # separate entities and n-grams
    text_no_marks = cleaner(text, min_len, 
                            rm_stopwords = False, 
                            rm_punctuation = True, 
                            rm_singlechars = False, 
                            rm_whitespaces = True)
    
    frequency_ngram = {e : text_no_marks.count(e) for e in entities}
    count_ngram = {e : 1 for e in entities}
           
    for ngram in considered_ngrams:   
         
        extra = 0
        
        freq = counts[ngram] + extra
        
        # update the frequency if an unigram is inside an entity BUT is not an entity itself
        # it may occur depending on max_ngram_size
        
        if any(ngram in e for e in entities):             
            for e in entities:            
                if ngram.lower() in e.lower() and ngram != 'e' and ngram != e: 
                    frequency_ngram[e] += freq   
                    count_ngram[e] += 1
                    
        else:
            frequency_ngram[ngram] = freq 
            
    for e in count_ngram.keys():
        if len(e.split()) > 1:
            frequency_ngram[e] /= sqrt(count_ngram[e]) # ex weighting_function
        
    frequency_ngram = {n : frequency_ngram[n]*sqrt(tf_idf(n, sentences_no_marks)) for n in frequency_ngram.keys()}
    
    # used for ranking the ngrams
    ranking_ngrams = {}
            
    for ngram, freq in frequency_ngram.items():  
                        
        # number of words in the ngram        
        cleaned_ngram = complete_clean(ngram, min_len)
        
        n = len([w for w in ngram.lower().split() if w.lower() not in stop_words])
        
        for r in replacer.keys():
            cleaned_ngram = cleaned_ngram.replace(r, replacer[r])
        
        # due to some problem(s) in spaCy, capitalized / decapitalized words are treated in different ways
        # causing OOV names of people to be assigned a null similarity
        # take the maximum similarity from all the possible combinations
            
        sp_ngram_std = nlp(cleaned_ngram)
        sp_ngram_low = nlp(cleaned_ngram.lower())
            
        s1 = sp_ngram_std.similarity(sp_text_cleaned_standard)
        s2 = sp_ngram_std.similarity(sp_text_cleaned_lowered)            
        s3 = sp_ngram_low.similarity(sp_text_cleaned_standard)
        s4 = sp_ngram_low.similarity(sp_text_cleaned_lowered)
            
        S = max([s1, s2, s3, s4])
        
        if printer:
            print(ngram, S)
        
        # bonus assignment
                
        bonus_start = 1
        bonus_lda = 1
        bonus_entity = 1
        bonus_sum = 1
        
        bonus_start *= bonus_sentence_staggering(ngram, 
                                                 cleaned_ngram,
                                                 first_sentences,                                    
                                                 first_sentences_cleaned, 
                                                 bonus_first_sentences, 
                                                 stagger)
        
        if any(snow.stem(s).lower() in ngram.lower() or s in ngram for s in words_from_gensim):
            bonus_lda *= bonus_gensim 
            
        if ngram in specials:
            bonus_entity *= bonus_special
                
        if ngram in entities:
            
            if printer:
                print(f'Entity {ngram} size {n}')
            
            # gives the bonus_entities if
            # - the entity is composed of at least 2 words 
            #   OR
            # - the entity contains a (proper) noun
            
            if n >= 2:          
                bonus_entity *= bonus_entities
            elif any(t.pos_ in ['PROPN', 'NOUN'] for t in nlp(ngram)):
                bonus_entity *= bonus_entities
            
            if ngram.lower() in tv_programs:
                bonus_entity *= bonus_special
            
            if any(wn == ws for ws in summary_cleaned.split() for wn in ngram.split()):
                bonus_sum *= bonus_summary
            
            if S < trim_sim:
                if  _min != _max and S > _min:
                    
                    if printer:
                        print('- To be trimmed:', ngram)
                    
                    S -= _min
                    S /= (_max - _min)
                    
        if snow.stem(ngram) in summary_cleaned:
            bonus_sum *= bonus_summary 
            
        if ngram == 'oroscopo' or ngram == 'previsioni' or ngram == 'previsioni zodiacali':
            if found_month:
                ngram += f' {the_month}'
                        
        ranking_ngrams[ngram] = (S, freq, n, bonus_entity, bonus_start, bonus_sum, bonus_lda)

    keyngrams = sort_dict_by_value(ranking_ngrams)
    
    if printer:
        print()
        print('Ranked Dictionary')
        for n, p in keyngrams.items():
            print(n, p)
    
    # if a smaller ngram is inside any of the (bigger) ones in the neighborhood (check constants.py)
    # then remove the smaller ngram
        
    kws = list(keyngrams.keys())
    for i, kw in enumerate(kws):
        
        try:            
            if any(kw in kws[i-x] for x in neighborhood) or any(kw in kws[i+x] for x in neighborhood):
                del keyngrams[kw]
            
        except IndexError:
            pass
            
    raw_weighted_keywords = []
    chosen_keywords = []
       
    # keyword selection
    
    for ngram, (score, parameters) in keyngrams.items():
                
        words = set(word_tokenize(ngram.lower()))
        
        if any(len(words.intersection(set(word_tokenize(kw.lower())))) >= frac*len(words) 
               for kw in chosen_keywords):
            continue
        
        if ngram in entities:             
            if all(ngram not in kw for kw in chosen_keywords):
                raw_weighted_keywords.append((ngram, score, parameters))
                chosen_keywords.append(ngram)
        
        else:   
            sp_ngram = nlp(ngram)
            
            for tk in sp_ngram:
                
                tk_text = tk.text
                
                if len(tk_text) < min_len:
                    continue
                
                if ((tk == sp_ngram[-1] or tk == sp_ngram[0]) and tk.pos_ in ['CCONJ', 'NUM', 'PUNCT']) or\
                   (tk == sp_ngram[-1] and tk.pos_ in ['VERB', 'PUNCT']):
                    continue
                    
                else:
                    st = snow.stem(tk_text.lower())
                    
                    if all(st != snow.stem(_w) for kw in chosen_keywords for _w in kw.split()):
                        raw_weighted_keywords.append((tk_text, score, parameters))
                        chosen_keywords.append(tk_text)
    
    if printer:
        print()
        print('RAW')
        pretty_print_keywords(raw_weighted_keywords)
    
    # join keywords into keyphrases if they have the same score
    
    weighted_keywords = []
    sentence = ''
    w_mem = 0
    parameters_mem = ()
    
    for i in range(0, len(raw_weighted_keywords)):   
        
        kw_0, w_0, parameters_0 = raw_weighted_keywords[i]
                
        try:
            kw_1, w_1, parameters_1 = raw_weighted_keywords[i+1]
        except:
            if len(sentence) > min_len:               
                weighted_keywords.append((sentence[:-1], w_mem, parameters_mem))
            else:
                weighted_keywords.append((kw_0, w_0, parameters_0))
            
            continue
        
        if isclose(w_0, w_1, abs_tol = 1e-4): # needed for the finite precision representation of Python     
            w_mem = w_0     
            parameters_mem = parameters_0
            
            if kw_0 not in sentence:
                sentence += kw_0 + ' '        
            
            if kw_1 not in sentence:
                sentence += kw_1 + ' ' 
            
            if i == len(weighted_keywords) - 1:
                weighted_keywords.append((sentence[:-1], w_mem, parameters_mem)) 
                
        else:
            if len(sentence) > min_len:
                
                weighted_keywords.append((sentence[:-1], w_mem, parameters_mem))
                sentence = kw_1 + ' '
                w_mem = w_1
                parameters_mem = parameters_1
                
            else:
                weighted_keywords.append((kw_0, w_0, parameters_0))
                
    if printer:
        print()
        print('REFIN')
        pretty_print_keywords(weighted_keywords)
    
    # check if remaining words still contain at least one gensim word and updates the ranking
    
    new_ranking_keywords = {}    
    
    for (expression, score, parameters) in weighted_keywords:
        
        final_params = list(parameters)
        
        if not(any(snow.stem(s).lower() in expression.lower() or s in expression for s in words_from_gensim)):
            final_params[-1] = 1
        
        if any(wn == ws for wn in expression.split() 
               for ws in summary_cleaned.split()) or snow.stem(expression) in summary_cleaned:
            final_params[-2] = bonus_summary
        else:
            final_params[-2] = 1
        
        new_ranking_keywords[expression] = tuple(final_params)
           
    new_ranking_keywords = sort_dict_by_value(new_ranking_keywords)
        
    keywords = [(expression, score, parameters) for expression, (score, parameters) in new_ranking_keywords.items()]  
    
    if printer:
        print()
        print('NEW RANKING')
        pretty_print_keywords(keywords)

    final_keywords = [(k, score, parameters) 
                      for (k, score, parameters) in keywords 
                      if 
                      ((k in entities and (nlp(k)[0].pos_ in ['NOUN', 'PROPN', 'ADJ', 'DET', 'ADV'])) or
                       any(t.pos_ in ['NOUN', 'PROPN'] for t in nlp(k))) and
                      all(t.pos_ != 'AUX' for t in nlp(k)) and
                      len(k) >= min_len]
    
    try:
        max_score = final_keywords[0][1]      
        final_keywords = [(k, 100*score/max_score, parameters) for (k, score, parameters) in final_keywords]
    except Exception as E:
        print(E)
        print(sentences)
        pretty_print_keywords(keywords)
    
    if printer:
        print()
        print('FINAL')
        pretty_print_keywords(final_keywords[:5])
                        
    return final_keywords  